module.exports = {
	plugins: ['stylelint-order', 'stylelint-config-rational-order/plugin'],
	extends: [
		'stylelint-config-sass-guidelines',
		'stylelint-config-rational-order',
		'stylelint-config-standard',
		'stylelint-config-css-modules'
	],
	rules: {
		'indentation': "tab",
		'at-rule-no-unknown': null,
		'no-descending-specificity': null,
		'block-no-empty': null,
		'no-empty-source': null,
		'number-leading-zero': null,
		'selector-class-pattern': null,
		'max-nesting-depth': 4,
		'selector-max-compound-selectors': 5,
		'order/properties-order': [],
		'order/properties-alphabetical-order': null,
		'function-parentheses-space-inside': null,
		'selector-pseudo-class-no-unknown': [
			true,
			{
				'ignorePseudoClasses': ['global', 'local']
			}
		],
		'plugin/rational-order': [
			true,
			{
				'border-in-box-model': false,
				'empty-line-between-groups': false
			}
		],
		'selector-no-qualifying-type': [ true, {
			'ignore': [ 'attribute' ]
		} ]
	},
}
