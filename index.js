const eslintConfig = require('./eslint')
const prettierConfig = require('./prettier')
const stylelintConfig = require('./stylelint')

module.exports = {
  esConfig: eslintConfig.esConfig,
  tsConfig: eslintConfig.tsConfig,
  prettierConfig,
  stylelintConfig
}
