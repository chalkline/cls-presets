# @root/packages

Common configuration for `eslint`, `pretier` and `stylelint`

## Eslint 
You can check `eslint` configuration for [es6](https://bitbucket.org/chalkline/cls-v4-pacakges/src/main/presets/eslint/config.es.js)
and [typescript](https://bitbucket.org/chalkline/cls-v4-pacakges/src/main/presets/eslint/config.ts.js)

### Eslint usage

1. Create `.eslintrc.js` file in the project root
2. Extend your config from the common `@d-i-p/presets/eslint`
```javascript
module.exports = {
  extends: require.resolve('@root/packages/presets/eslint/config.ts'),
}
```

## Prettier

You can check `prettier` configuration [here](https://bitbucket.org/chalkline/cls-v4-pacakges/src/main/presets/prettier/index.js)

### Prettier usage

1. Create `prettier.config.js` file in the project root
2. Extend your config from the common `@root/packages/presets/prettier`
```javascript
const defaultPrettierConfig = require('@root/packages/presets/prettier');
module.exports = {
    ...defaultPrettierConfig,
    // your custom config goes here  
}
```

## Stylelint

You can check `stylelint` configuration [here](https://bitbucket.org/chalkline/cls-v4-pacakges/src/main/presets/stylelint/index.js)

1. Install latest `stylint` in your project (otherwise some older verion brought by other modules will report errors)
2. First of all you need create `.stylelintrc` file in the root of your folder
3. Extend your config from the common `@root/packages/presets/stylelint` (dont use `require` as the values are auto-required)
```javascript
{
  "extends": [
    "@root/packages/presets/stylelint"
  ]
}
```
