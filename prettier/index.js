module.exports = {
	bracketSpacing: true,
	printWidth: 90,
	semi: false,
	singleQuote: true,
	tabWidth: 4,
	trailingComma: 'es5',
	useTabs: true,
	bracketSameLine: false,
	endOfLine: 'auto',
}
