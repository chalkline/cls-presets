module.exports = {
	parser: '@typescript-eslint/parser',
	parserOptions: {
		ecmaVersion: 2020,
		sourceType: 'module',
		ecmaFeatures: {
			jsx: true,
		},
	},
	plugins: ['compat', 'jsx-a11y', 'react-hooks', 'import'],
	rules: {
		'react/no-unescaped-entities': ['error', { forbid: ['>', '}'] }],
		'react/no-typos': ['error'],
		'react/jsx-no-bind': [
			'warn',
			{
				ignoreDOMComponents: true,
				ignoreRefs: false,
				allowArrowFunctions: true,
				allowFunctions: false,
				allowBind: false,
			},
		],
		'no-undef': ['error'],
	},
	extends: [
		'plugin:react/recommended',
		'plugin:@typescript-eslint/recommended',
		'plugin:react-hooks/recommended',
		'plugin:sonarjs/recommended'
	],
}
