const tsConfig = require('./config.ts')
const esConfig = require('./config.es')

module.exports = {
	tsConfig,
	esConfig,
}
